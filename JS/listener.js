$(document).ready(cargar);

var id = sessionStorage.getItem('id');
var a = "" + (parseInt(id) - 1);
var receta = localStorage.getItem(id);
var guion = receta.split('_');
var ingredientes = guion[3].split('&');
var pasos = guion[4].split('%');
var ulIngredientes = document.getElementById('ingre');
var ulPasos = document.getElementById('dire');

const ani = document.getElementById('#container');
const tl = new TimelineMax();

tl.fromTo(ani, 1, { height: "0%" }, { height: "80%", ease: Power2.easeInOut }).fromTo(ani, 1.2, { width: "100%" }, { width: "80%", ease: Power2.easeInOut });


function cargar() {
    $("#img").css("height", "600px");
    $("#img").css("background", "url(IMG/" + a + ".jpg) no-repeat center center fixed");
    $("#img").css("-webkit-background-size", "cover");
    $("#img").css("-moz-background-size", "cover");
    $("#img").css("-o-background-size", "cover");
    $("#img").css("background-size", "cover");
    document.getElementById("descripcion").innerHTML = guion[2];
    for (var i = 0; i < ingredientes.length - 1; i++) {
        var ingrediente = ingredientes[i];
        var li = document.createElement("li");
        li.classList.add("list-group-item");
        li.appendChild(document.createTextNode(ingrediente));
        ulIngredientes.appendChild(li);
        console.log(localStorage.length);
    }

    for (var i = 0; i < pasos.length - 1; i++) {
        var paso = pasos[i];
        var li = document.createElement("li");
        li.classList.add("list-group-item");
        li.appendChild(document.createTextNode(paso));
        ulPasos.appendChild(li);
    }
}
/*height: 600 px;
background: url(IMG / ivan - torres - MQUqbmszGGM - unsplash.jpg) no - repeat center center fixed; 
-webkit - background - size: cover; 
-moz - background - size: cover; 
-o - background - size: cover;
background - size: cover;*/